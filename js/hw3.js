// Теоретичні питання
// 1. Цикл використовується, щоб повторити якусь однотипну дію. 

// 2. Коли, наприклад, потрібно вивести у консоль числа від 1 до 20, замість того, 
// щоб писати console.log() 20 разів, ми користуємось циклом.

// 3. Неявне перетворення типів:
// alert("Good day" + 144 + '');

// let 22 = null;

// Явне перетворення: 
// let a = false;
// alert(Boolean(a)) -> false;

// let h = 1228;
// alert(typeof(h)) -> Number;

// let G = null;
// alert(sypeof(G)) -> null;


// Завдання.
let number = prompt("Write any number");
if (isNaN(number) || number === null || number < 5){
    console.log("Sorry, no numbers");
}
for(let i = 0; i <= number; i++){
    if (i % 5 === 0){
        console.log(i);
}
}
